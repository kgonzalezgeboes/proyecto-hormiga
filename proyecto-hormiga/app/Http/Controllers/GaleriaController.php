<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GaleriaController extends Controller
{
    public function cargarView() {
        // Obtener imagenes automáticamente de la carpeta
        $imagenes = Storage::disk('public')->allFiles("galeria");

        return view('galeria', ["imagenes" => $imagenes]);
    }
}
