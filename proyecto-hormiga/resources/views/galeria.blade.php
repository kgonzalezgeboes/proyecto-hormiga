@extends('layouts.plantilla')
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Galeria</title>

    <!-- Vendor CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
        media="screen">

    <style>
        body {
            background-color: #1d1d1d !important;
            font-family: "Asap", sans-serif;
            color: #989898;
            margin: 10px;
            font-size: 16px;
        }

        #demo {
            height: 100%;
            position: relative;
            overflow: hidden;
        }


        .green {
            background-color: #6fb936;
        }

        .thumb {
            margin-bottom: 30px;
        }

        .page-top {
            margin-top: 85px;
        }


        div.zoom {
            min-height: 200px !important;
            -webkit-transition: all .3s ease-in-out;
            -moz-transition: all .3s ease-in-out;
            -o-transition: all .3s ease-in-out;
            -ms-transition: all .3s ease-in-out;
        }

        .fancybox img {
            width: 100%;
            height: 200px;
            border-radius: 5px;
            object-fit: cover;
        }


        .transition {
            -webkit-transform: scale(1.2);
            -moz-transform: scale(1.2);
            -o-transform: scale(1.2);
            transform: scale(1.2);
        }

        .modal-header {

            border-bottom: none;
        }

        .modal-title {
            color: #000;
        }

        .modal-footer {
            display: none;
        }

    </style>
</head>

<body>
    <h1 class="text-center text-white mt-5">Galería</h1>

    <div class="container mt-5">
        <div class="row">
            @foreach ($imagenes as $imagen)
                @php
                    // Obtener nombre de la especie en base a la URL
                    $sp = str_replace('-', ' ', $imagen);
                    $sp = str_replace(['.jpg', 'galeria/'], '', $sp);
                @endphp

                <div class="col-lg-3 col-md-4 col-xs-6 thumb zoom">
                    <a href="/storage/{{ $imagen }}" class="fancybox" rel="ligthbox">
                        <img src="/storage/{{ $imagen }}" class="img-fluid" alt="">
                    </a>
                    <p class="text-center text-white">@php echo $sp; @endphp</p>
                </div>
            @endforeach
        </div>
    </div>

    <!-- Vendor JS -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

    <script>
        $(document).ready(function() {
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });

            $(".zoom").hover(function() {

                $(this).addClass('transition');
            }, function() {

                $(this).removeClass('transition');
            });
        });

    </script>
</body>

</html>
